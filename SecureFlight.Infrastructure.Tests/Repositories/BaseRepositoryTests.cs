using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SecureFlight.Core.Entities;
using SecureFlight.Infrastructure.Repositories;
using System.Linq;

namespace SecureFlight.Infrastructure.Tests.Repositories
{
    public class BaseRepositoryTests
    {
        private SecureFlightDbContext _context;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<SecureFlightDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase")
            .Options;
            _context = new SecureFlightDbContext(options);
            _context.Database.EnsureCreated();
        }

        [Test]
        public void Update_NewEntity_Success()
        {
            var newAirport = new Airport
            {
                City = "CDMX",
                Code = "AAQ",
                Country = "MExico",
                Name = "Mexico airport"
            };

            var baseRepository = new BaseRepository<Airport>(_context);

            // Action
            baseRepository.Update(newAirport);

            // Assert
            var updatedAirport = _context.Airports.First(x => x.Code.Equals(newAirport.Code));
            Assert.AreEqual(newAirport.City, updatedAirport.City);
            Assert.AreEqual(newAirport.Country, updatedAirport.Country);
            Assert.AreEqual(newAirport.Name, updatedAirport.Name);
        }
    }
}